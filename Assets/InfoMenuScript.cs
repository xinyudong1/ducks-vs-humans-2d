using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoMenuScript : MonoBehaviour
{
    public void GoBack_BTN_Click()
    {
        MenuManager.Instance.HideMenu(MenuManager.Instance.InfoMenu);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.MainMenu);
    }
}
