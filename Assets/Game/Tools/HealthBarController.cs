using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarController : MonoBehaviour
{
    // HealthBar
    RectTransform healthGreenBar;
    [SerializeField] RectTransform fullHealthBar;

    float healthBarMaxWidth;


    private void Awake()
    {
        healthGreenBar = GetComponent<RectTransform>();
        healthBarMaxWidth = healthGreenBar.rect.width;
    }

    public void CalculateHealthBar(float health, float max_health)
    {
        float healthRatio = Mathf.Clamp(health / max_health, 0.0f, 1.0f);

        healthGreenBar.sizeDelta = new Vector2(healthBarMaxWidth * healthRatio, healthGreenBar.sizeDelta.y);
    }
}
