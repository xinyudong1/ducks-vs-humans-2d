using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public SceneReference UIScene;

    public bool ShowUI = true;
    public MenuClassifier UIClassifier;
    public CameraClassifier UICameraClassifier;

    private void Start()
    {
        Scene scene = SceneManager.GetSceneByPath(UIScene);
        if (scene.isLoaded == false)
        {
            SceneLoader.Instance.OnSceneLoadedEvent += SceneLoadedCallback;
            SceneLoader.Instance.LoadScene(UIScene, false);
        }
        else if (scene.buildIndex == -1)
        {
            Debug.Assert(false, $"Scene no found {UIScene}");
        }
        else
        {
            SceneLoadedCallback(null);
        }
    }

    void SceneLoadedCallback(List<string> scenesLoaded)
    {
        SceneLoader.Instance.OnSceneLoadedEvent -= SceneLoadedCallback;

#if UNITY_EDITOR
        if (ShowUI)
        {
            MenuManager.Instance.ShowMenu(UIClassifier);
        }
        else
        {
            MenuManager.Instance.HideMenu(UIClassifier);
        }
#else
        MenuManager.Instance.ShowMenu(UIClassifier);
#endif
        CameraManager.Instance.DisableCamera(UICameraClassifier);
    }
}
