using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraObject : MonoBehaviour
{
    public CameraClassifier cameraClassifier;

    private bool isEnabled = false;

    public enum StartingCameraStates
    {
        Ignore,
        Active,
        Disable
    };

    public StartingCameraStates StartingState;

    public bool IsEnabled
    {
        get
        {
            return isEnabled;
        }

        set
        {
            isEnabled = value;
            gameObject.SetActive(isEnabled);
        }
    }

    public virtual void OnEnabled()
    {
        IsEnabled = true;
    }

    public virtual void OnDisabled()
    {
        IsEnabled = false;
    }
    protected virtual void Start()
    {
        CameraManager.Instance.AddCameraObject(this);

        switch (StartingState)
        {
            case StartingCameraStates.Active:
                isEnabled = true;
                gameObject.SetActive(true);
                break;

            case StartingCameraStates.Disable:
                isEnabled = false;
                gameObject.SetActive(false);
                break;
        }
    }

    private void OnDestroy()
    {
        CameraManager.Instance.RemoveCameraObject(this);
    }
}
