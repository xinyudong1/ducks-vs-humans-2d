using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CameraClassifier", menuName = "Cameras/Camera Classifier")]
public class CameraClassifier : ScriptableObject
{
}
