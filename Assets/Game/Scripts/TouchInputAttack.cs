using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.UI;

public class TouchInputAttack : Singleton<TouchInputAttack>
{
    public GameObject duckChoice = null;
    public GameObject towerChoice = null;

    public Camera mainCamera;
    TouchInputs touchInputs;

    // Start is called before the first frame update
    void Awake()
    {
        

        touchInputs = new TouchInputs();

        touchInputs.Touch.Touch_0.started += FireRaycast;
    }

    private void FireRaycast(InputAction.CallbackContext obj)
    {
        TouchState touch = obj.ReadValue<TouchState>();
    }


    private void Update()
    {
        if (mainCamera == null)
            return;
        CheckRaycasts();
    }

    private void CheckRaycasts()
    {
        if (touchInputs.Touch.Touch_0.phase == InputActionPhase.Performed)
        {
            TouchState touch = touchInputs.Touch.Touch_0.ReadValue<TouchState>();
            Vector3 raytest = mainCamera.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 10.0f));
            Ray ray = new Ray(raytest, Vector3.up);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 10.0f))
            {
                if (hit.collider.gameObject.GetComponent<DuckLogic>())
                {
                    duckChoice = hit.collider.gameObject;
                }
                if (hit.collider.gameObject.GetComponent<TowerLogic>())
                {
                    towerChoice = hit.collider.gameObject;
                }
                if (hit.collider.gameObject.GetComponent<BaseController>())
                {
                    towerChoice = hit.collider.gameObject;
                }
            }
        }
    }

    //Send the new target to the relevant DuckLogic
    public void ConfirmAttack()
    {
        duckChoice.GetComponent<DuckLogic>().ConfirmedNewTargetfromUI(towerChoice.GetComponent<PhotonView>().ViewID);
    }

    private void OnEnable()
    {
        touchInputs.Enable();
    }
    private void OnDisable()
    {
        touchInputs.Disable();
    }

    //private void OnDrawGizmos()
    //{
    //        Gizmos.color = Color.red;
    //        Gizmos.DrawLine(ray.origin, ray.origin + ray.direction * 100.0f);
    //}
}
