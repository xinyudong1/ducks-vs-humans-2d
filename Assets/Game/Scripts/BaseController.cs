using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseController : MonoBehaviourPun
{
    [SerializeField] HealthBarController healthBar;
    private float health;
    private float base_health;
    public Action<float, float> onHealthChanged;

    private void Start()
    {
        onHealthChanged += healthBar.CalculateHealthBar;

    }

    [PunRPC]
    private void TakeDamageBaseRPC(float _damage)
    {
        health -= _damage;

        if (onHealthChanged != null)
            onHealthChanged(health, base_health);

    }

    [PunRPC]
    private void SetStartingHealthBase(float _health)
    {
        base_health = _health;
        health = base_health;
    }
}
