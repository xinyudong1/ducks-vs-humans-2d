using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUDScript : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI timerText;
    
    public void SpawnMeleeDuck()
    {
        DuckSpawnManager.Instance.SpawnMeleeDuck();
    }

    public void SpawnMedicDuck()
    {
        DuckSpawnManager.Instance.SpawnMedicDuck();
    }

    public void SpawnScoutDuck()
    {
        DuckSpawnManager.Instance.SpawnScoutDuck();
    }
    public void SpawnSniperDuck()
    {
        DuckSpawnManager.Instance.SpawnSniperDuck();
    }

    public void Pause()
    {
        MenuManager.Instance.HideMenu(MenuManager.Instance.HudMenu);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.PauseMenu);
    }

    //Code for Targeting System Integration into the HUD begins here

    public GameObject attackPanel;
    public GameObject duckImage;
    public GameObject TowerImage;

    private void FixedUpdate()
    {
        if (TouchInputAttack.Instance.mainCamera == null)
            return;
        

        if (TouchInputAttack.Instance.duckChoice == null && TouchInputAttack.Instance.towerChoice == null)
        {
            attackPanel.SetActive(false);
        }
        else
            attackPanel.SetActive(true);

        
        if (TouchInputAttack.Instance.duckChoice != null)
        {
            duckImage.SetActive(true);
            duckImage.GetComponent<Image>().sprite = TouchInputAttack.Instance.duckChoice.GetComponent<SpriteRenderer>().sprite;
        }
        else
            duckImage.SetActive(false);
        
        if (TouchInputAttack.Instance.towerChoice != null)
        {
            TowerImage.SetActive(true);
            TowerImage.GetComponent<Image>().sprite = TouchInputAttack.Instance.towerChoice.GetComponent<SpriteRenderer>().sprite;
        }
        else
            TowerImage.SetActive(false);
    }

    public void ConfirmAttack()
    {
        TouchInputAttack.Instance.ConfirmAttack();
    }

    [PunRPC]
    private void SetTimer(int time)
    {
        // get the time from time seconds to a time span minutes:seconds
        TimeSpan timeSpan = TimeSpan.FromSeconds(time);
        timerText.text = timeSpan.ToString("mm':'ss");

    }

}
