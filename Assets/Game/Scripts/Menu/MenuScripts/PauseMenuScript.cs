using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Photon.Pun;

[RequireComponent(typeof(PauseMenu))]
public class PauseMenuScript : MonoBehaviourPunCallbacks
{
    [SerializeField] private SceneReference GameScene;

    public void ResumeButton_Click()
    {
        MenuManager.Instance.HideMenu(MenuManager.Instance.PauseMenu);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.HudMenu);
    }

    public void MainMenuButton_Click()
    {
        MenuManager.Instance.HideMenu(MenuManager.Instance.PauseMenu);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.MainMenu);

        PhotonNetwork.Disconnect(); // TODO Change to LeaveRoom/LeaveLoby later

        SceneLoader.Instance.UnloadScene(GameScene);

        CameraManager.Instance.EnableCamera(CameraManager.Instance.UISceneCamera);
    }
}
