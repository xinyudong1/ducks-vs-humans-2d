using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class MainLobbyController : MonoBehaviourPunCallbacks
{
    [SerializeField] private TextMeshProUGUI loadingText;

    [SerializeField] private TMP_InputField joinRoomText;
    [SerializeField] private TMP_InputField createRoomText;

    [SerializeField] private TextMeshProUGUI errorText;

    [SerializeField] private GameObject lobbyUI;


    public override void OnEnable()
    {
        base.OnEnable();
        loadingText.gameObject.SetActive(true);
        loadingText.text = "Connecting to Lobby...";
        errorText.gameObject.SetActive(false);
        joinRoomText.text = "";
        createRoomText.text = "";
        if (PhotonNetwork.CurrentLobby is not null)
        {
            ShowLobby();
            return;
        }
        
        PhotonNetwork.JoinLobby();
    }
    
    private void ShowLobby()
    {
        loadingText.gameObject.SetActive(false);
        lobbyUI.SetActive(true);
    }

    public override void OnJoinedLobby()
    {
        ShowLobby();
    }

    public void CreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions();
       
        PhotonNetwork.CreateRoom(createRoomText.text, new RoomOptions { MaxPlayers = 3 });
        loadingText.text = "Creating Room...";
        lobbyUI.SetActive(false);
    }

    public void JoinRoom()
    {
        if (joinRoomText.text == "")
        {
            errorText.text = "Please enter a room name";
            errorText.gameObject.SetActive(true);
            return;
        }
        PhotonNetwork.JoinRoom(joinRoomText.text);
        loadingText.text = "Connecting to Room...";
        lobbyUI.SetActive(false);
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        MenuManager.Instance.HideMenu(MenuManager.Instance.GameLobbyMenu);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.RoomLobbyMenu);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);
        errorText.text = message;
        errorText.gameObject.SetActive(true);
        lobbyUI.SetActive(true);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        errorText.text = message;
        errorText.gameObject.SetActive(true);
        lobbyUI.SetActive(true);
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);

        Debug.Log("List of Available Rooms:");

        if (roomList.Count == 0)
        {
            Debug.Log("No Rooms Available");
            return;
        }

        foreach (RoomInfo room in roomList)
        {
            Debug.Log(room.Name);
        }
    }
}
