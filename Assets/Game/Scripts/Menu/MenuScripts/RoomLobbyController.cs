using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RoomLobbyController : MonoBehaviourPunCallbacks
{
    [SerializeField] TextMeshProUGUI loadingText;
    [SerializeField] TextMeshProUGUI waitForPlayersText;
    [SerializeField] TextMeshProUGUI playersText;
    [SerializeField] private SceneReference gameScene;
    List<Player> players;


    public override void OnEnable()
    {
        base.OnEnable();
        players = new List<Player>(PhotonNetwork.PlayerList);
        if (players.Count > 3)
        {
            PhotonNetwork.LeaveRoom();
        }

        foreach (Player player in players)
        {
            // leave if another 2D player is in the game
            if (player.NickName.Split(":")[0] == "2D Player" && player.IsLocal == false)
            {
                Debug.LogError("Another 2D player is in the game");
                PhotonNetwork.LeaveRoom();
            }
            
        }
        
        DisplayPlayers();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        players.Add(newPlayer);
        DisplayPlayers();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        players.Remove(otherPlayer);
        
        
        DisplayPlayers();
    }

    private void DisplayPlayers()
    {
        playersText.text = "";
        foreach (Player player in players)
        {
            Debug.Log(player.NickName);
            string[] playerName = player.NickName.Split(':');
            string name = "";
            for (int i = 1; i < playerName.Length; i++)
            {
                name += playerName[i];
            }
            playersText.text = playersText.text + name + '\n';
        }
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        MenuManager.Instance.HideMenu(MenuManager.Instance.RoomLobbyMenu);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.GameLobbyMenu);
    }

    [PunRPC]
    private void StartGameRPC()
    {
        MenuManager.Instance.HideMenu(MenuManager.Instance.RoomLobbyMenu);
        CameraManager.Instance.DisableCamera(CameraManager.Instance.UISceneCamera);
        PhotonNetwork.LoadLevel(gameScene, LoadSceneMode.Additive);
        //SceneLoader.Instance.LoadScenePhoton(gameScene, true);
        
    }
}
