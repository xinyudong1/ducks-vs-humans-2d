using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="PlayerInfo", menuName = "ScriptableObject/Player")]
public class PlayerInfo : ScriptableObject
{
    public string NickName;
}
