using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;

public class CameraController : MonoBehaviour
{
    [SerializeField] Camera mainCamera;
    TouchInputs touchInputs;
    [SerializeField] float scaleFactor;
    [SerializeField] float moveFactor;

    private void Awake()
    {
        touchInputs = new TouchInputs();

    }
    void Start()
    {
        touchInputs.Touch.Touch_0.started += Drag;
    }

    private void Drag(InputAction.CallbackContext obj)
    {
        // check for a pinch
        if (touchInputs.Touch.Touch_1.ReadValue<TouchState>().delta.sqrMagnitude > 0.1f)
            return;


    }

    void Update()
    {
        CheckPinch();

        CheckDrag();
    }

    void CheckPinch()
    {
        if (touchInputs.Touch.Touch_0.phase == InputActionPhase.Performed
            //&&
            //touchInputs.Touch.Touch_1.phase == InputActionPhase.Performed
            )
        {
            TouchState touch0 = touchInputs.Touch.Touch_0.ReadValue<TouchState>();
            TouchState touch1 = touchInputs.Touch.Touch_1.ReadValue<TouchState>();

            if (touch0.delta.sqrMagnitude <= 0.2f || touch1.delta.sqrMagnitude <= 0.2f)
            {
                return;
            }
            Vector2 touch0PrevPos = touch0.position - touch0.delta;
            Vector2 touch1PrevPos = touch1.position - touch1.delta;

            float prevTouchDeltaMag = (touch0PrevPos - touch1PrevPos).magnitude;
            float touchDeltaMag = (touch0.position - touch1.position).magnitude;

            float deltaMagnitudeDifference = prevTouchDeltaMag - touchDeltaMag;

            mainCamera.GetComponent<Camera>().orthographicSize += deltaMagnitudeDifference * Time.deltaTime * scaleFactor;

        }

    }

    void CheckDrag()
    {
        if (touchInputs.Touch.Touch_0.phase == InputActionPhase.Performed)
        {
            TouchState touch0 = touchInputs.Touch.Touch_0.ReadValue<TouchState>();
            TouchState touch1 = touchInputs.Touch.Touch_1.ReadValue<TouchState>();

            if (touch1.delta.sqrMagnitude > 0.1f) // pinching
                return;
            
            Vector2 touch0PrevPos = touch0.position - touch0.delta;

            float touchDeltaMag = (touch0.position - touch1.position).magnitude;

            mainCamera.transform.position = new Vector3(
                mainCamera.transform.position.x - touch0.delta.x * Time.deltaTime * moveFactor,
                mainCamera.transform.position.y,
                mainCamera.transform.position.z - touch0.delta.y * Time.deltaTime * moveFactor
                );
        }
    }

    private void OnEnable()
    {
        touchInputs.Enable();
    }

    private void OnDisable()
    {
        touchInputs.Disable();
    }
}
