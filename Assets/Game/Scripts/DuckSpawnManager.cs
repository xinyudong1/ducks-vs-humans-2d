using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckSpawnManager : Singleton<DuckSpawnManager>
{
    public Transform duckSpawnLocation;
    public Vector3 DuckSpawnLocation;

    private void Awake()
    {
        if(duckSpawnLocation != null)
            DuckSpawnLocation = duckSpawnLocation.position;
    }

    public void SpawnMeleeDuck()
    {
        GameManager.Instance.NetworkManager2D.SpawnDuck(NetworkManager2D.DUCKTYPE.MELEE, DuckSpawnLocation);
    }

    public void SpawnMedicDuck()
    {
        GameManager.Instance.NetworkManager2D.SpawnDuck(NetworkManager2D.DUCKTYPE.MEDIC, DuckSpawnLocation);
    }

    public void SpawnScoutDuck()
    {
        GameManager.Instance.NetworkManager2D.SpawnDuck(NetworkManager2D.DUCKTYPE.SCOUT, DuckSpawnLocation);
    }
    public void SpawnSniperDuck()
    {
        GameManager.Instance.NetworkManager2D.SpawnDuck(NetworkManager2D.DUCKTYPE.RANGED, DuckSpawnLocation);
    }
}
