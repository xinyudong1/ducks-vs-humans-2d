using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreController : Singleton<CoreController>
{
    public SceneReference CoreScene;
    
    public bool ShowMainMenu = true;
    public bool DisableUICamera = false;
    public bool ShowHUDMenu = false;

    public void LoadCoreScene()
    {
        ShowMainMenu = false;
        DisableUICamera = true;
        ShowHUDMenu = true;

        SceneLoader.Instance.LoadScene(CoreScene, false);
    }

    public void LoadCoreSceneMainMenu()
    {
        ShowMainMenu = true;
        DisableUICamera = false;
        ShowHUDMenu = false;
        
        SceneLoader.Instance.LoadScene(CoreScene, false);
    }

    //private void Awake()
    //{
    //    if (Instance != null && Instance != this)
    //        Destroy(gameObject);

    //    DontDestroyOnLoad(gameObject);
    //}
}