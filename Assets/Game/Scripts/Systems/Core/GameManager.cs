using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] GameObject NetworkManager2DPrefab;

    [HideInInspector] public NetworkManager3D NetworkManager3D;

    [HideInInspector] public NetworkManager2D NetworkManager2D;

    //public SpriteRenderer mapSprite;

    void Start()
    {
        //Debug.Assert(mapSprite != null, $"{gameObject.name}: Map Sprite is missing!");
        PhotonNetwork.Instantiate(NetworkManager2DPrefab.name, Vector3.zero, Quaternion.identity, 0);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.HudMenu);
    }
}
