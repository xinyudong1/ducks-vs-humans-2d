using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class TowerSpawnManager : Singleton<TowerSpawnManager>
{
    [Serializable]
    public enum TowerType
    {
        Projectile,
        Mining,
        AOE
    }

    [PunRPC]
    void SpawnTower(TowerType towerType, Vector3 position)
    {
        return;
    }
}
