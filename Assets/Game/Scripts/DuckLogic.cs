using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckLogic : MonoBehaviourPun
{
    [SerializeField] HealthBarController healthBar;
    private float health;
    private float base_health;
    public GameObject targetTower;
    public Action<float, float> onHealthChanged;

    private void Start()
    {
        onHealthChanged += healthBar.CalculateHealthBar;
    }


    //Recieves the targetTower from 3D to 2D side here
    [PunRPC]
    private void SendTargetTo2DRPC(int viewID)
    {
        targetTower = PhotonView.Find(viewID).gameObject;
    }

    //Send the new Target set by user here to 3D side and fire the RPC 
    public void ConfirmedNewTargetfromUI(int viewID)
    {
        Debug.Log("New Target of "+ this.gameObject.name + " is " +PhotonView.Find(viewID).gameObject.name);
        photonView.RPC("ConfirmedNewTargetRPC", RpcTarget.All, viewID);
    }

    [PunRPC]
    private void ConfirmedNewTargetRPC(int viewID)
    {
        targetTower = PhotonView.Find(viewID).gameObject;
    }

    [PunRPC]
    private void TakeDamageDuckRPC(float _damage)
    {
        health -= _damage;

        if (onHealthChanged != null)
            onHealthChanged(health, base_health);
        
    }

    [PunRPC]
    private void SetStartingHealthDuck(float _health)
    {
        base_health = _health;
        health = base_health;
    }

    [PunRPC]
    private void ApplyHealDuckRPC(float _heal)
    {
        health += _heal;

        if (onHealthChanged != null)
            onHealthChanged(health, base_health);
    }
}
