using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;

public class TapController : MonoBehaviour
{
    [SerializeField] Camera mainCamera;
    TouchInputs touchInputs;
    [SerializeField] float scaleFactor;
    [SerializeField] float moveFactor;

    private void Awake()
    {
        touchInputs = new TouchInputs();
        touchInputs.Touch.Touch_0.started += PlaceUnit;

    }

    private void PlaceUnit(InputAction.CallbackContext obj)
    {
        // check if a unit is selected

        // If a unit is selected, send a message to the server to spawn a unit at that location
        TouchState touch0 = obj.ReadValue<TouchState>();

        // deselect the unit
    }

    private void OnEnable()
    {
        touchInputs.Enable();
    }

    private void OnDisable()
    {
        touchInputs.Disable();
    }
}
