using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ConnectToServer : MonoBehaviourPunCallbacks
{
    [SerializeField] TextMeshProUGUI loadingText;
    [SerializeField] private PlayerInfo playerInfo;

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to master");

        PhotonNetwork.NickName = $"2D Player:{playerInfo.NickName}";
        MenuManager.Instance.HideMenu(MenuManager.Instance.LoadNetworkMenu);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.GameLobbyMenu);
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("Joined lobby");
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnEnable()
    {
        base.OnEnable();

        ConnectToPun();
    }

    public void ConnectToPun()
    {
        if (PhotonNetwork.InRoom || PhotonNetwork.InLobby)
            return;

        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.Disconnect();
            Debug.Log("Disconnected from the network");
        }

        Debug.Log("Trying to connect to the PUN network...");
        PhotonNetwork.ConnectUsingSettings();
    }
}
