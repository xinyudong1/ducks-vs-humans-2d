using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager3D : MonoBehaviourPun
{
    private void Start()
    {
        GameManager.Instance.NetworkManager3D = this;
    }


    [PunRPC]
    private void HumansWin()
    {
        CameraManager.Instance.EnableCamera(CameraManager.Instance.UISceneCamera);
        SceneLoader.Instance.UnloadScene(SceneLoader.Instance.GameScene);
        MenuManager.Instance.HideMenu(MenuManager.Instance.HudMenu);
        MenuManager.Instance.HideMenu(MenuManager.Instance.PauseMenu);
        
        MenuManager.Instance.ShowMenu(MenuManager.Instance.LoseScreen);
    }

    [PunRPC]
    private void DucksWin()
    {
        CameraManager.Instance.EnableCamera(CameraManager.Instance.UISceneCamera);
        SceneLoader.Instance.UnloadScene(SceneLoader.Instance.GameScene);
        MenuManager.Instance.HideMenu(MenuManager.Instance.HudMenu);
        MenuManager.Instance.HideMenu(MenuManager.Instance.PauseMenu);

        MenuManager.Instance.ShowMenu(MenuManager.Instance.WinScreen);
    }
}
