using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager2D : MonoBehaviourPun
{
    public enum DUCKTYPE
    {
        MELEE,
        MEDIC,
        SCOUT,
        RANGED
    }

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.NetworkManager2D = this;
        // send a ChatMessage RPC to all clients with the message "Hello World"
        photonView.RPC("ChatMessage", RpcTarget.All, "Hello from ", PhotonNetwork.NickName);

    }

    [PunRPC]
    void ChatMessage(string a, string b)
    {
        Debug.Log(string.Format("ChatMessage {0} {1}", a, b));
    }

    public void SpawnDuck(DUCKTYPE _type, Vector3 _position)
    {
        photonView.RPC("SpawnDuckRPC", RpcTarget.All, _type, _position);
    }
    // SpawnUnit RPC that will send a message with a unit GameObject and position
    [PunRPC]
    void SpawnDuckRPC(DUCKTYPE type, Vector3 position)
    {
        Debug.Log("SpawnDuck from the 2D side");

    }

}
