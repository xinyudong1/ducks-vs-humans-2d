using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraAddtoTargetingSystem : MonoBehaviour
{
    Camera mainCamera;
    private void Awake()
    {
        mainCamera = GetComponent<Camera>();
    }
    private void Start()
    {
        TouchInputAttack.Instance.mainCamera = mainCamera;
    }

}
